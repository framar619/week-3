import java.util.Scanner;

public class Exceptions {
    public static float doDivision(float num1, float num2) {
        float output = num1 / num2;
        return output;
    }

    public static boolean number1(String num1) {
        try {
            Float.parseFloat(num1);
            return true;
        } catch (Exception e) {
            System.out.println("The number is not a valid number");
            return false;
        }

    }

    public static boolean iszero(String num2) {
        try {
            float value = Float.parseFloat(num2);
            if (value == 0.0f) {
                System.out.println("The number is not divisible by 0");
                return true;
            } else
                return false;
        } catch (Exception e) {
            System.out.println("The number is not a valid number");
            return true;
        }
    }

    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            String num1;
            String num2;
            do {
                System.out.print("Input first number: ");
                num1 = in.nextLine();
            } while (!number1(num1));

            do {
                System.out.print("Input second number: ");
                num2 = in.nextLine();
            } while (iszero(num2));


            float var1 = doDivision(Float.parseFloat(num1), Float.parseFloat(num2));
            System.out.println(var1);

        } catch (Exception e) {
            System.out.print(e);
        }
    }

}

